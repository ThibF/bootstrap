# Bootstrap
When an embedded board is completely empty, a method is required to load it up
with software to be functional. To accomplish this a few basic components are
needed. The bootstrapping server requires a dedicated Ethernet device and will
use the default address **192.168.163.1/24**.

## Using this repository
This repository is not intended to be used directly. While perfectly adequate to
do so, every project may be different. This repository should rather be
considered a rather bare template. Never the less, pull-requests always welcome.

Note, in the scripts subdirectory a script 'install.sh' is found. This script
can be used to install this repository to a local system. It currently only
works with Standard Debian. Warning, the script will modify the way networking
behaves and without a second network interface, tasks that require a internet
connection (to update etc) will not work without this.

### Releases and tagging
To create a release, an annotated tag is to be used. A tag shall have the form
of 'vX.Y[.Z][-rcN]'.
* X required major version of the release, can be any integer value, including a year
* Y required minor version of the release, can be any integer value, including month
* Z optional hotfix version of the release only used in release branches
* N optional release candidate of the release, can be any integer value

To create a release branch, tag the master branch with the major and minor
components only.
So for example *v0.10*. The CI will then create the needed branches and tags
on a successful build.

It is strongly preferred to use the local ***git_tag_release.sh*** script, as it
not only wraps around the regular git tag command, but also updates the
*CHANGELOG.md* file.

An important note about merging, the CI will do updates and merges across all
branches based from the stable/vX.Y/releases branch. As such, merges into the
release branches should be avoided, as conflicts cannot be resolved by the CI.

To avoid race conditions, ensure that not two tags are pushed before other
tasks have finished in the CI. This mostly is a problem when many concurrent
tasks are enabled and thus run in parallel on the CI.

The recommended strategy is thus to fork from master by creating the branch tag,
and fixes that need to be applied to other branches, for example a fix already
on master, is to be cherry picked. Fixes from a release branch back to master
should follow the normal branch -> merge (into master) principle.

#### Release candidates
The release branch will also be taged with an release candidate status.
For the previous example of release tag *v0.10* a tag *v0.10.0-rc1* will be
created.

#### Final release
If after a certain amount of time, the release candidate is deemed ready for
release, it shall be tagged with its release tag. For example *v0.10.0* to mark
*v0.10.0-rc1* as to be released. In other words, the -rc tag is to be removed.

#### Hotfixes
Within the main release branch (stable/v0.10/library) hotfix patches can be
applied.
If a release candidate is desired for a certain number of hotfixes, tag the
release branch as such, for example *v0.10.0-rc1*. If there has been a hotfix
release already, then the numbering should take this into account. For example
*v0.10.1-rc0* and so if another release candidate is to follow the tag would be
*v0.10.1-rc1*

 To create a new release with these hotfixes, a new tag is to be created
in the form of *v0.10.1* to release *v0.10.1-rc1*.

#### Annotated tags
Annotated tags are required, as all scripts will ignore light tags. The tag
subject will be used as the release name, and the tag body will be used as
release notes. As such it is strongly recommended to use messaged tags.

#### Automated changelogs
The CHANGELOG.md file is to be updated automatically using the
`git_tag_release.sh` script, optionally after creating the desired tag.

The `git_tag_release.sh` script will take the tag and its message, generate
the relevant 'CHANGELOG.md' file if needed and commit it and rewrite the tag to
point to the new commit. All arguments passed are those of git tag. If no
arguments are passed, the current HEAD will be evaluated if it points to a tag
and if not, fail.

Note, that only full tags are considered in the changelog, not any '-rc' tag.

#### Hooks
As hosted environment do not allow installing pre-receive hooks, we can only
ensure proper tags are pushed. A proper tag always points to an commit updating
the 'CHANGELOG.md' file. Included in this repository is a git *pre-push* hook
that checks if the supplied tag follows this rule. Everybody that has the
ability and permission to create any release tag (see above) will
need to either do this completely manually (not recommended) or use the
preferred method by installing the supplied *pre-push* hook from the '.githooks'
directory by either manually copying the file to '.git/hooks' or tell git to use
the .githooks directory instead allowing for version managed and updated hooks
(strongly recommended).
```console
git config --local core.hooksPath .githooks
```

## Wrapper
Included in this repository is script called 'docker_bootstrap.sh'. This script
can be used to simplify building the container, running docker with all its
arguments.

### Prerequisites
To ensure that the included 'docker_bootstrap.sh' script can run without
problems the script will perform a self-test at start. If there are missing
dependencies, the script will print an error and inform about what was missing.

### Usage
To simplify things more, a symlink can be put into the local path. For example
in *~/bin* or */usr/local/bin*. In the following example the script is
optionally renamed. Care is to be taken however when renaming to avoid conflicts
with other files.
```console
$ ln -s /path/to/docker_bootstrap.sh ~/bin/docker_bootstrap.sh
```

Then, the script can be called from the directory containing the binary files to
be hosted. For example using the hosts second Ethernet device *eth2*:
```console
$ cd /directory/holding/binary_output/
$ docker_bootstrap.sh -i eth2
```

Alternatively the *-p* parameter can be passed to the script containing the
directory to be served.
```console
$ docker_bootstrap.sh -i eth2 -p /directory/holding/binary_output/
```

Note, that files to be served by TFTP need to be in a sub-directory called
*tftpboot*, e.g. '/directory/holding/binary_output/tftpboot/'. Files to be
served via HTTP need to be in a sub-directory called *www*, e.g.
'/directory/holding/binary_output/www/'. Files to be served via NFS need
to be in a sub-directory called *nfs*, eg
'/directory/holding/binary_output/nfs/'.

If the 'www' is not a directory, then the 'tftpboot' directory will be used
to serve the files instead.

### NFS support
The *-n* parameter can also be used to the script containing the directory
to be served over NFS.
```console
$ docker_bootstrap.sh -i eth2 -n directory/containing/nfs
```

If the 'nfs' is not a directory, then the 'tftpboot' directory will be used
to serve the files instead.

Note that this directory can not be inside an ecryptfs directory, needs to be a
relative path from the start directory (due to docker volumes), and this NFS
share will only available on the bootstraps default network.

### Options
The wrapper script can pass additional environment variables or mount points
into the container if needed. While using the option flags is straight forward,
passing more then one of these via environment variables can be tricky and
should be avoided.

Simply put however, any additional arguments will not be pre-processed and are
basically passed straight through to docker.

## Building
To create the bootstrap environment, simply build the local docker container,
or use the one from the registry.
```console
$ docker build --rm -t bootstrap:latest .
```
Take note of the used tag, 'bootstrap:latest' as this will be needed when
running the container explained below. Also note that any name can be chosen
as long as this is done consistently.

## Running
Running the container has a few notes to be very careful about. First, the
container requires access to configure the network hardware and do network
broadcast. As such, *privileged* mode is required.

Further more, the container needs access to a physical network port. On laptops
this can be realized by using the fixed Ethernet for the container and WiFi for
normal network connectivity, if so required. Alternatively a second Ethernet
device can be added to the host also.

Finally, the files to be served via the tftp server. This is best accomplished
by being in the directory that contains the files to be hosted.

For example using the hosts second Ethernet device, *eth2* would look something
like this.
```console
$ cd /directory/holding/binary_output/
$ docker run \
    --network host \
    --privileged
    --rm \
    -e DHCPD_IFACE="eth2" \
    -i \
    -t \
    -v "$(pwd):/bootstrap"
    bootstrap:latest
```

This container can also be run in the background on a dedicated server. This is
purposely left as an exercise to the reader.

## Debugging
Sometimes things do not go as expected and then it helps to be able to
investigate things.

### Running container
For a running container, `docker exec`
can be used. First, obtain the name of the container. `docker container ls`
will yield the name in the last column. Alternatively the *--name* parameter can
be supplied to docker run. Then use this name to start a program in the running
container.
```console
$ docker exec -it bootstrap-container /bin/sh
```
Will start a shell inside the container named 'bootstrap-container'. The *-it*
parameters are used to execute an *interactive* command on a *terminal* and is
only needed for a shell.

### No container
If there is no container started, simply append the command to run to the docker
run command from above. So by appending '/bin/sh' we get a shell inside said
container. No programs will be started up so that will have to be done manually,
for example by executing the commands from */init*.

### Virtual Machine
Supplied is a basic Vagrantfile that can be used with `vagrant up` to create
a Virtual machine. A few manual steps are still required however, such as adding
a second network adapter and running the actual installation (which can only be
done after adding the second adapter). The adding of the network adapter can not
be done automatically due to the fact that it is best to use the pass-through
option here which would be different for every system. Two providers are
supported, libvirt-kvm and virtualbox.
