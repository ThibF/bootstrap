# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>

stages:
  - lint
  - build
  - test
  - branch
  - update
  - release
  - deploy
  - complete

# Common parameters
# ===========================================================================
.common_parameters: &common_parameters
  tags:
    - docker

.changes_docker: &changes_docker
  changes:
    - .dockerignore
    - Dockerfile
    - dockerfiles/**/*


# Linting
# =============================================================================
.linting:
  <<: *common_parameters
  stage: lint

shellscript_linting:
  extends: .linting
  image: registry.hub.docker.com/koalaman/shellcheck-alpine:stable
  only:
    changes:
      - ./**/*.sh
      - ./.git-hooks/*
  script:
    - find . -iname '*.sh' -o -ipath './.git-hooks/*' -exec shellcheck -C -f tty -s sh "{}" \;

dockerfile_linting:
  extends: .linting
  image: registry.hub.docker.com/hadolint/hadolint:latest-debian
  only:
    changes:
      - Dockerfile
  script:
    - find '.' -iname 'Dockerfile' -exec hadolint -f tty "{}" \;


# Build
# ===========================================================================
.docker:
  <<: *common_parameters
  only:
    <<: *changes_docker
  image: registry.hub.docker.com/library/docker:stable

build:
  extends: .docker
  stage: build
  script:
    - docker build --pull --rm -t "${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}" .

test_image:
  extends: .docker
  image: "${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}"
  stage: test
  script:
    - "/test/buildenv_check.sh"


# Release branches
# ===========================================================================
.git:
  <<: *common_parameters
  image: registry.hub.docker.com/gitscm/git:latest
  before_script:
    - git config --local user.name "${GITLAB_USER_NAME}"
    - git config --local user.email "${GITLAB_USER_EMAIL}"
    - git config --local credential.helper "cache --timeout=2147483647"
    - printf "url=${CI_PROJECT_URL}\nusername=esbs-bot\npassword=${CI_PERSONAL_TOKEN}\n\n" | git credential approve
    - git remote set-url --push origin "https://esbs-bot@${CI_REPOSITORY_URL#*@}"
  after_script:
    - git credential-cache exit
  variables:
    GIT_DEPTH: "0"

create_release_branches:
  extends: .git
  stage: branch
  only:
    refs:
      - /^v\d+\.\d+$/
  except:
    refs:
      - branches # Workaround for gitlab-org/gitlab-ce#27818
  script:
    - git_tag="$(git describe --exact-match --match "v[0-9]*")"
    - git_tag_msg="$(git tag --format="%(contents:subject)%0a%0a%(contents:body)%0a%0a(Auto-created release candidate)" --list "${git_tag}")"
    - |
      for release_branch in "release" $(git branch --list --remote "origin/stable/v0/*" | sed -n -r 's|.*origin/stable/v0/||p'); do
        echo "Creating release branch '${release_branch}' from '${CI_COMMIT_REF_NAME}' (${CI_COMMIT_SHORT_SHA})"
        git checkout "${CI_COMMIT_SHA}" -b "stable/${CI_COMMIT_TAG:?}/${release_branch}"
        if [ "${release_branch}" = "release" ]; then
          git tag --annotate --message="${git_tag_msg:-See tag '${CI_COMMIT_TAG}'}" "${release_prefix:-}${CI_COMMIT_TAG}.0-rc1"
        else
          sed -i 's|FROM registry.hub.docker.com/library/alpine|FROM registry.hub.docker.com/'"${release_branch}"'/alpine|g' "Dockerfile"
          git add "Dockerfile"
          git commit --message="${release_branch}: Auto-created release branch 'stable/${CI_COMMIT_TAG:?}/${release_branch}'." --no-edit
          release_prefix="${release_branch}/"
        fi
        git push --follow-tags origin "HEAD"
      done

update_arch_release_branches:
  extends: .git
  stage: update
  only:
    refs:
      - /^stable/v\d+\.\d+/release$/ # Workaround for gitlab-org/gitlab-ce#27818 only for branches
  except:
    refs:
      - tags
  script:
    - git_tag_msg="$(git tag --format="%(contents:subject)%0a%0a%(contents:body)" --list "${CI_COMMIT_TAG}")"
    - git_release_tag="$(echo "${CI_COMMIT_REF_NAME}" | sed -n -r 's|^(stable/)?(v[[:digit:]]+\.[[:digit:]]+).*$|\2|p')"
    - |
      for release_branch in $(git branch --list --remote "origin/stable/v0/*" | sed -n -r 's|.*origin/stable/v0/||p'); do
        echo "Merging updates for '${release_branch}' from '${CI_COMMIT_REF_NAME}' (${CI_COMMIT_SHORT_SHA})"
        git checkout "stable/${git_release_tag}/${release_branch}"
        git reset --hard "origin/stable/${git_release_tag}/${release_branch}"
        git merge --log --no-edit --no-ff "${CI_COMMIT_SHA}"
        git push origin "HEAD"
      done

create_arch_release_tags:
  extends: .git
  stage: release
  only:
    refs:
      - /^v\d+(\.\d+){2,}(-rc\d+)?$/ # Workaround for gitlab-org/gitlab-ce#27818 only for tags
  except:
    refs:
      - branches
  script:
    - git_tag_msg="$(git tag --format="%(contents:subject)%0a%0a%(contents:body)" --list "${CI_COMMIT_TAG}")"
    - git_release_tag="$(echo "${CI_COMMIT_REF_NAME}" | sed -n -r 's|^(stable/)?(v[[:digit:]]+\.[[:digit:]]+).*$|\2|p')"
    - |
      for release_branch in $(git branch --list --remote "origin/stable/v0/*" | sed -n -r 's|.*origin/stable/v0/||p'); do
        echo "Merging updates for '${release_branch}' from '${CI_COMMIT_REF_NAME}' (${CI_COMMIT_SHORT_SHA})"
        git checkout "stable/${git_release_tag}/${release_branch}"
        git reset --hard "origin/stable/${git_release_tag}/${release_branch}"
        git tag --annotate --force --message="${git_tag_msg:-See tag '${CI_COMMIT_TAG}'}" "${release_branch}/${CI_COMMIT_TAG}"
        git push --follow-tags origin "HEAD"
      done


# Deploy containers
# ===========================================================================
.deploy:
  extends: .docker
  stage: deploy
  before_script:
    - docker login --password "${CI_JOB_TOKEN}" --username gitlab-ci-token "${CI_REGISTRY}"

deploy_builds:
  extends: .deploy
  only:
    <<: *changes_docker
    refs:
      - /^stable/v\d+\.\d+/.*$/
  except:
    refs:
      - tags # Workaround for gitlab-org/gitlab-ce#27818
  script:
    - |
      if [ "${CI_COMMIT_REF_NAME%*/release}" != "${CI_COMMIT_REF_NAME}" ]; then
        docker_arch="library"
      else
        docker_arch="${CI_COMMIT_REF_NAME##*/}"
      fi
    - docker tag  "${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}" "${CI_REGISTRY_IMAGE}/${docker_arch}/${CI_PROJECT_NAME}:${CI_COMMIT_SHORT_SHA}"
    - docker push "${CI_REGISTRY_IMAGE}/${docker_arch}/${CI_PROJECT_NAME}:${CI_COMMIT_SHORT_SHA}"


deploy_release:
  extends: .deploy
  only:
    <<: *changes_docker
    refs:
      - /^(.*/)?v\d+(\.\d+){2,}$/
  except:
    refs:
      - branches # Workaround for gitlab-org/gitlab-ce#27818
  script:
    - |
      if [ "${CI_COMMIT_TAG#*/*}" != "${CI_COMMIT_TAG}" ]; then
        docker_arch="${CI_COMMIT_TAG%%/*}"
      else
        docker_arch="library"
      fi
    - docker_tag="${CI_COMMIT_TAG##*/}"
    - docker tag  "${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}" "${CI_REGISTRY_IMAGE}/${docker_arch}/${CI_PROJECT_NAME}:${docker_tag:?}"
    - docker push "${CI_REGISTRY_IMAGE}/${docker_arch}/${CI_PROJECT_NAME}:${docker_tag}"
    - docker tag  "${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}" "${CI_REGISTRY_IMAGE}/${docker_arch}/${CI_PROJECT_NAME}:${docker_tag%.*}"
    - docker push "${CI_REGISTRY_IMAGE}/${docker_arch}/${CI_PROJECT_NAME}:${docker_tag%.*}"
    - |
      repository="$(wget -q -O - \
                      --header "PRIVATE-TOKEN: ${CI_PERSONAL_TOKEN}" \
                      "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories" | \
                    sed \
                      -e 's|[][]||g' \
                      -e 's|[[:space:]]||g' \
                      -e 's|"||g' \
                      -e 's|}|\n|g' \
                      -e 's|,{|{|g' | \
                    sed -n '/name:'"${docker_arch}"'\/'"${CI_PROJECT_NAME}"'/p' | \
                    sed -n -r 's|.*id:([[:digit:]]+),.*|\1|p' | \
                    head -n 1)"
      latest_major_minor="$(wget -q -O - \
                      --header "PRIVATE-TOKEN: ${CI_PERSONAL_TOKEN}" \
                      "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories/${repository}/tags"| \
                    sed \
                      -e 's|[][]||g' \
                      -e 's|[[:space:]]||g' \
                      -e 's|"||g' \
                      -e 's|}|\n|g' \
                      -e 's|,{|{|g' | \
                    sed -n -r 's|.*name:v([[:digit:]]+\.[[:digit:]]+).*|\1|p' | \
                    sort -n -r -t '.' -u | \
                    head -n 1)"
      latest_major="${latest_major_minor%%.*}"
      docker_tag_major="${docker_tag%%.*}"
      if [ "${docker_tag_major#v*}" -lt "${latest_major?}" ]; then
        return
      fi
      latest_minor="${latest_major_minor#*.}"
      docker_tag_minor_patch="${docker_tag#${docker_tag_major}.}"
      docker_tag_minor="${docker_tag_minor_patch%.*}"
      docker_tag_patch="${docker_tag_minor_patch#*.}"
      if [ "${docker_tag_major#v*}" -eq "${latest_major?}" ] && \
         [ "${docker_tag_minor}" -lt "${latest_minor?}" ]; then
          return
      fi
      echo "Pushing ${docker_tag} as latest"
      docker tag  "${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}" "${CI_REGISTRY_IMAGE}/${docker_arch}/${CI_PROJECT_NAME}:latest"
      docker push "${CI_REGISTRY_IMAGE}/${docker_arch}/${CI_PROJECT_NAME}:latest"


# Complete
# ===========================================================================
announce_release:
  <<: *common_parameters
  image: registry.hub.docker.com/olliver/curl:latest # Workaround until this is merged upstream
  stage: complete
  when: delayed
  start_in: 86 minutes
  before_script:
    - apk add --no-cache git # Workaround for gitlab-org/gitlab-ce#59608
  only:
    <<: *changes_docker
    refs:
      - /^v\d+\.\d+(\.\d+)+$/
  except:
    refs:
      - branches # Workaround for gitlab-org/gitlab-ce#27818
  script:
    - CI_COMMIT_TAG_MESSAGE="$(git tag --format="%(contents:subject)%0a%0a%(contents:body)" --list ${CI_COMMIT_TAG:?})" # Workaround for gitlab-org/gitlab-ce#59608
    - | # Workaround for gitlab-org/gitlab-ce#59726
      CI_COMMIT_TAG_MESSAGE="$(echo "${CI_COMMIT_TAG_MESSAGE}" | \
                               tr -d '\b' | \
                               tr '\f' '\n' | \
                               sed \
                                 -e 's|\\|\\\\|g' \
                                 -e 's|"|\\"|g' \
                                 -e 's|/|\\/|g' \
                                 -e ':a;N;$!ba;s|\n|\\n|g' \
                                 -e 's|\r|\\r|g' \
                                 -e 's|\t|\\t|g')"
    - |
      curl \
        --data "{\"tag_name\": \"${CI_COMMIT_TAG}\", \"name\": \"${CI_PROJECT_NAME} ${CI_COMMIT_TAG}\", \"description\": \"${CI_COMMIT_TAG_MESSAGE:-No release notes.}\"}" \
        --fail \
        --header "Content-Type: application/json" \
        --header "Private-Token: ${CI_PERSONAL_TOKEN}" \
        --output "/dev/null" \
        --request POST \
        --show-error \
        --silent \
        --write-out "HTTP response: %{http_code}\n\n" \
        "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases"

cleanup_docker_containers:
  extends: .docker
  stage: complete
  when: always
  script:
    - |
      if docker inspect --type image "${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}" 1> "/dev/null"; then
        docker rmi "${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}"
      fi
