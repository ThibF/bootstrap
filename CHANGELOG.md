# Changelog
**NOTE:** DO NOT EDIT! This changelog is automatically generated.
See the README.md file for more information

## v1.1 (2019-08-14)
  - Add NFS support
  - bug: Update dhcp config default path

## v1.0 (2019-08-09)
  - Add support for sitara 335x series bootrom
  - Add a few simple convenience functions
  - Create basic bootstrapping infrastructure
