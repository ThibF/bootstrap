#!/usr/bin/dumb-init /bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2019 EVBox B.V.
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

set -eu

IP_ADDR="${IP_ADDR:-192.168.163.1}"
NFS_ROOT="${SHARED_DIRECTORY:-/bootstrap/nfs/}"
TFTP_ROOT="/bootstrap/tftpboot/"
WWW_ROOT="/bootstrap/www/"

e_err()
{
    >&2 echo "ERROR: ${*}"
}

e_warn()
{
    echo "WARN: ${*}"
}

if ! ip address replace "${IP_ADDR}/24" dev "${DHCPD_IFACE:?}"; then
    e_err "Unable to configure network device '${DHCPD_IFACE}'."
    exit 1
fi
if ! ip route replace "${IP_ADDR%.*}.0/24" dev "${DHCPD_IFACE:?}"; then
    e_err "Unable to configure route for device '${DHCPD_IFACE}'."
    exit 1
fi

if ! syslogd; then
    e_warn "Failed to start syslog, logging may not work."
fi

sleep 1

if [ ! -d "${WWW_ROOT}" ]; then
    WWW_ROOT="${TFTP_ROOT}"
fi
if ! httpd -h "${WWW_ROOT}" -p "${IP_ADDR}"; then
    e_warn "Failed to tart httpd server."
fi

if ! in.tftpd --address "${IP_ADDR}" --listen --secure --verbosity=7 "${TFTP_ROOT}"; then
    e_err "Failed to start in.tftpd."
    exit 1
fi

if ! iperf3 --bind "${IP_ADDR}" --daemon --logfile "/var/log/iperf3.log" --server; then
    e_warn "Failed to start iperf3 server."
fi

sed -i 's|@IP_ADDR@|'"${IP_ADDR}"'|g' "/etc/ntpd.conf"
if ! ntpd; then
    e_warn "Failed to start ntpd server."
fi

if [ ! -d "${NFS_ROOT:-}" ]; then
    NFS_ROOT="${TFTP_ROOT}"
fi
if [ -d "${NFS_ROOT:-}" ]; then
    PERMITTED="${PERMITTED:-${IP_ADDR%.*}.*}" \
    SHARED_DIRECTORY="${NFS_ROOT}" \
    "/usr/bin/nfsd.sh" &
fi

sed -i 's|@IP_PREFIX@|'"${IP_ADDR%.*}"'|g' "/etc/dhcp/dhcpd.conf"
if ! dhcpd "${DHCPD_IFACE:?}"; then
    e_err "Failed to start dhcpd."
    exit 1
fi

tail -f "/var/log/messages" "/var/log/iperf3.log" &

chmod go-rwx "/etc/logrotate.conf"

while true; do
    chmod a+rX -R "${TFTP_ROOT}"
    sleep 3600
    if ! logrotate "/etc/logrotate.conf"; then
        e_warn "Logrotation failed."
    fi
done

exit 0
