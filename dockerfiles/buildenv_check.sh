#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>

set -eu

REQUIRED_COMMANDS="
    [
    cat
    command
    dhcpd
    echo
    exit
    exportfs
    getopts
    httpd
    inetd
    ip
    iperf3
    kill
    logrotate
    ntpd
    pidof
    printf
    rpc.mountd
    rpc.nfsd
    rpcbind
    rpcinfo
    sed
    set
    shift
    sleep
    syslogd
    tail
    tftpd
    trap
"


usage()
{
    echo "Usage: ${0} [OPTIONS]"
    echo "A simple test script to verify the environment has all required tools."
    echo "    -h  Print usage"
}

check_requirements()
{
    for cmd in ${REQUIRED_COMMANDS}; do
        if ! test_result="$(command -V "${cmd}")"; then
            test_result_fail="${test_result_fail:-}${test_result}\n"
        else
            test_result_pass="${test_result_pass:-}${test_result}\n"
        fi
    done

    echo "Available commands:"
    # As the results contain \n, we expect these to be interpreted.
    # shellcheck disable=SC2059
    printf "${test_result_pass:-none\n}"
    echo
    echo "Missing commands:"
    # shellcheck disable=SC2059
    printf "${test_result_fail:-none\n}"
    echo

    if [ -n "${test_result_fail:-}" ]; then
        echo "Command test failed, missing programs."
        test_failed=1
    fi
}

main()
{
    while getopts ":h" options; do
        case "${options}" in
        h)
            usage
            exit 0
            ;;
        :)
            echo "Option -${OPTARG} requires an argument."
            exit 1
            ;;
        ?)
            echo "Invalid option: -${OPTARG}"
            exit 1
            ;;
        esac
    done
    shift "$((OPTIND - 1))"

    check_requirements

    if [ "${test_failed:-0}" -ne 0 ]; then
        echo "Test(s) failed."
        exit 1
    fi

    echo "All Ok"
}

main "${@}"

exit 0
