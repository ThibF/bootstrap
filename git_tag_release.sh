#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

set -eu

DEF_BRANCH="master"


recreate_tag()
{
    _git_tag_msg="$(git tag --format="%(contents:subject)" --list "${git_tag}")"
    _git_tag_body="$(git tag --format="%(contents:body)" --list "${git_tag}")"
    if [ -n "${_git_tag_body}" ]; then
        _git_tag_msg="$(printf "%s\n\n%s" "${_git_tag_msg:-}" "${_git_tag_body}")"
    fi

    _git_tag_msg="$(
	cat <<-EOT
		${_git_tag_msg}

		${git_changelog}
	EOT
    )"
    echo "Appending changelog to tag:"
    echo "${_git_tag_msg}"

    _git_tag_signature="$(git tag --format="%(contents:signature)" --list "${git_tag}")"
    if [ -n "${_git_tag_signature}" ]; then
        echo "Signature on current tag found, re-signing required."
        _git_tag_resign="true"
    fi
    git tag --annotate --force --message="${_git_tag_msg}" ${_git_tag_resign+--sign} "${git_tag}"
}

update_changelog()
{
    _changelog="${1:?}"
    _changelog_header="$(
		cat <<-EOT
			# Changelog
			**NOTE:** DO NOT EDIT! This changelog is automatically generated.
			See the README.md file for more information
		EOT
    )"
    # Tail needs + 1, so add a \n to get an extra line
    _changelog_header_size="$(printf "%s\n\n" "${_changelog_header}" | wc -l)"

    if [ -z "${_changelog}" ]; then
        return
    fi

    if [ -f "CHANGELOG.md" ]; then
        mv "CHANGELOG.md" "CHANGELOG.md.orig"
        trap 'mv "CHANGELOG.md.orig" "CHANGELOG.md"' EXIT
    fi

    if [ -f "CHANGELOG.md.orig" ] && \
       [ "$(wc -l < "CHANGELOG.md.orig")" -ge "${_changelog_header_size}" ]; then
        _changelog="${_changelog}$(printf "\n" && tail -n "+${_changelog_header_size}" "CHANGELOG.md.orig")"
    fi
    {
        echo "${_changelog_header}"
        echo
        echo "${_changelog}"
    } > "CHANGELOG.md"
    git add "CHANGELOG.md"
    if git diff-index --quiet "HEAD" -- "CHANGELOG.md"; then
        echo "No changes for 'CHANGELOG.md', done"
        exit 0
    fi
    echo "Updating CHANGELOG.md"
    git commit --message="Changelog: Auto-generate new CHANGELOG.md" --no-edit
    trap - EXIT
    if [ -f "CHANGELOG.md.orig" ]; then
        rm "CHANGELOG.md.orig"
    fi
}

generate_release_changelog()
{
    for _git_hash in $(git log \
                          --merges \
                          --pretty=tformat:"%H" "${git_previous_tag}"...HEAD); do
        _git_log_entries="$(printf "%s\n." "${_git_log_entries:-}  - $(git show --no-patch --pretty=format:"%b" "${_git_hash}" | head -n 1)")"
        _git_log_entries="${_git_log_entries%.}" # Trailing newlines get stripped by sub-shell command substitution
    done
    if [ -z "${_git_log_entries:-}" ]; then
        git_changelog="## No changes for ${git_tag} (${git_tag_date})"
    else
        git_changelog="$(printf "## ${git_tag} (%s)\n%s\n" "${git_tag_date}" "${_git_log_entries}")"
    fi
}

generate_hotfix_changelog()
{
    # No log is required for the 'main' release (v0.1.0)
    if [ "${git_tag##*.}" -eq 0 ] 2> "/dev/null"; then
        return
    fi

    _git_release_hotfixes="$(git log \
                                --no-merges \
                                --pretty=format:"  - %s" "${git_previous_tag}"...HEAD | \
                            sed -r '/^[[:space:]]*- Changelog:.*/d')"

    if [ -n "${_git_release_hotfixes:-}" ]; then
        git_changelog="$(
		cat <<-EOT
			## Hotfixes for ${git_tag} (${git_tag_date}):
			${_git_release_hotfixes}
		EOT
        )"
    else
        git_changelog="$(
		cat <<-EOT
			## No changes for ${git_tag} (${git_tag_date})
		EOT
        )"
    fi
}

main()
{
    git_branch="$(git rev-parse --abbrev-ref HEAD)"
    if [ "${git_branch}" != "${DEF_BRANCH}" ] && \
       [ -z "$(echo "${git_branch}" | sed -n -r '/^stable\/v[[:digit:]]+(\.[[:digit:]]+)+.*\/.*$/p')" ]; then
        echo "Release tags are only allowed on the default branch '${DEF_BRANCH}'"
        echo "and on stable release branches (stable/v*/[\$arch|release])."
        exit 1
    fi

    git_tag="$(git describe --exact-match --match "v[0-9]*" HEAD 2> "/dev/null" || true)"
    if [ -n "$(echo "${git_tag:-}" | sed -n -r '/^.*v[[:digit:]]+(\.[[:digit:]]+)+(-rc[[:digit:]]+)?$/p')" ] && \
       [ -z "$(echo "${git_tag:-}" | sed -n -r '/^.*v[[:digit:]]+(\.[[:digit:]]+)+(-rc[[:digit:]]+)$/p')" ]; then
        echo "Current 'HEAD' is already properly tagged."
        echo "Only release candidate tags (-rc) can be re-tagged."
        exit 1
    fi

    if [ "${#}" -ne 0 ]; then
        git tag "${@}"
    fi

    if ! git_tag="$(git describe --exact-match --match "v[0-9]*" HEAD 2> "/dev/null")"; then
        echo "Current 'HEAD' is not an annotated tag, nothing to do."
        exit 0
    fi

    while
        git_previous_tag="$(git describe --abbrev=0 --match "v[0-9]*" "${git_previous_tag:-HEAD~1}")"
            [ "${git_previous_tag%%-rc*}" != "${git_previous_tag}" ]; do
            git_previous_tag="${git_previous_tag}~1"
    done
    if [ -z "${git_previous_tag:-}" ]; then
        echo "Unable to find previous annotated tag."
        echo "On new projects, tag v0 on the last scaffolding commit'."
        echo "If so desired, after generating the changelog and first real tag"
        echo "The tag could be deleted before pushing."
        exit 1
    fi

    git_tag_date="$(git tag \
                        --format="%(taggerdate:short)" \
                        --list "${git_tag}")"

    # Get branch history since last release (e.g all merge commits since v0.10)
    # when tagging a new release branch.
    if [ "$(echo "${git_tag}" | sed -r 's|^v[[:digit:]]+\.[[:digit:]]+$||g')" != "${git_tag}" ]; then
        generate_release_changelog
    # Get branch history since the first commit of the release branch
    elif [ "$(echo "${git_tag}" | sed -r 's|^.*v[[:digit:]]+(\.[[:digit:]]+){2,}$||g')" != "${git_tag}" ]; then
        generate_hotfix_changelog
    fi

    if [ -n "${git_changelog:-}" ]; then
        update_changelog "${git_changelog}"
        recreate_tag "${git_changelog}"
    fi
}

main "${@}"

exit 0
